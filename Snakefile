#!/usr/bin/env python
import pandas as pd 
 

import os,sys 

configfile:"config.yaml" 
genomes=config["reference"] 
 
units = pd.read_csv("metadata.tsv", sep="\t", dtype = str).set_index(["sample","unit"], drop=False)
units.index.names = ["sample_id", "unit_id"]

index_columns = ["sample", "unit","fq1","fq2","control", 'text']
units = units.reindex(columns = index_columns)
replicat = units['sample'].value_counts()
##### wildcard constraints #####
wildcard_constraints:
    sample = "|".join(units["sample"]) ,
    unit = "|".join(units["unit"]),
    reference = "|".join(genomes.keys()) 


context = ["CpG","CHG","CHH"]
mapper = config["bismark"]["aligner"]
def is_single_end(sample, unit):
    """
    Determine whether unit is single-end or paired-end.
    
    A unit is single-end if the "fq2" column is null.
    """
    fq2_present = pd.notnull(units.loc[(sample, unit), "fq2"])
    return not fq2_present
def get_fastq(wildcards):
    """
    Returns a pandas Series of file paths to the input FASTQ files.

    If the unit is single-end, returns a single file path. If the unit is
    paired-end, returns a Series with two file paths.
    """
    if is_single_end(wildcards.sample, wildcards.unit):
        # Single-end unit: return the single file path
        return units.loc[(wildcards.sample, wildcards.unit), ["fq1"]].dropna()
    else:
        # Paired-end unit: return two file paths
        return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()

def get_fastp_files(wildcards):
    """
    Returns a single file path (for single-end) or a tuple of two file paths (for paired-end)
    to the trimmed FASTQ files.
    """
    if is_single_end(wildcards.sample, wildcards.unit):
        # Single-end unit: return the single file path
        return "results/trimmed/" + wildcards.sample + "-" + wildcards.unit + ".fastq.gz"
    else:
        # Paired-end unit: return two file paths
        return (
            "results/trimmed/" + wildcards.sample + "-" + wildcards.unit + "_R1.fastq.gz",
            "results/trimmed/" + wildcards.sample + "-" + wildcards.unit + "_R2.fastq.gz"
        )

 
###Can be given to increase mem on each crash
def get_mem_mb(wildcards, attempt):
    """
    Returns the memory (in MB) to request for the given rule invocation.

    If the rule invocation is on its first attempt, request 4GB of memory.
    If the rule invocation is on its second attempt, request 8GB of memory.
    If the rule invocation is on its third or later attempt, request 20GB of memory.
    """
    if(attempt == 1):
        # First attempt: request 4GB of memory
        return 4000
    elif(attempt == 2):
        # Second attempt: request 8GB of memory
        return 8000
    else:
        # Third or later attempt: request 20GB of memory
        return 20000

def get_multiqc_input(wildcards):
    multiqc_input = []
    for (sample, unit) in units.index:
        control_val = units.loc[(sample,unit)]["control"]
        if is_single_end(sample, unit):
            multiqc_input.extend(
                expand (
                    [
                        "results/trimmed/{sample}-{unit}_single.json",
                        "results/{reference}/{sample}-{unit}_bismark_bt2_PE_report.txt"
                    ],
                    sample = sample,
                    unit = unit,
                    reference= genomes.keys()
                )
            )
        else:
            multiqc_input.extend(
                expand (
                    [ 
                        "results/trimmed/{sample}-{unit}_paired.json",
                        "results/{reference}/{sample}-{unit}_R1_bismark_bt2_PE_report.txt",
                        "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.M-bias.txt",
                        "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated_splitting_report.txt",
                        "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplication_report.txt"
                    ],
                    sample = sample,
                    unit = unit,
                    reference= genomes.keys()
                )
            )


    return multiqc_input

def all_input(wildcards):
    wanted_input = [] 
    wanted_input.extend([
        "results/multiqc/multiqc.html"
    ])
    for (sample, unit) in units.index:
        control_val = units.loc[(sample,unit)]["control"]
        wanted_input.extend(
            expand(
                [
                    "results/{reference}/{sample}-{unit}_R1_bismark_bt2_PE_report.html",
                    "results/{reference}/{sample}-{unit}_coverage.bw" 
                ],
                sample = sample ,
                unit = unit,
                reference= genomes.keys()
            )
        )
        
        if sample != control_val:  
            wanted_input.extend(
                expand(
                    [ 
                     #   "results/{reference}/{context}_context_{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.txt.gz",
                     #   "results/{reference}/{context}_{sample}-{unit}.cov.gz.bismark.cov.gz", 
                        "results/{reference}/methylkit_results2/{sample}_{control}_{context}.bedGraph", 
                        "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hyper.bedGraph",
                        "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hypo.bedGraph" 
                    ],
                   # zip,
                    sample = sample ,
                    unit = unit,
                    control = units.loc[(sample,unit)]["control"],
                    reference= genomes.keys(),
                    context= ["CpG","CHG","CHH"]
                )
            )
            
    return wanted_input


onsuccess:
    from snakemake.report import auto_report
    auto_report(workflow.persistence.dag, "report/report.html")


rule all:
    input: all_input


rule genome_fasta:
    input:
        lambda wildcards: genomes[wildcards.reference]["assembly"]
    output:
        "results/{reference}/{reference}.fa"
    shell:
        "cp {input} {output}"

rule samtools_index:
    input:
        "results/{reference}/{reference}.fa"
    output:
        "results/{reference}/{reference}.fa.fai"
    envmodules:
        config["modules"]["samtools"]
    singularity:
        config["containers"]["samtools"]
    shell:"""
        samtools faidx {input}
    """

rule fastp_paired:
    input:
        get_fastq
    output:  
        read1="results/trimmed/{sample}-{unit}_R1.fastq.gz",
        read2="results/trimmed/{sample}-{unit}_R2.fastq.gz",
        json="results/trimmed/{sample}-{unit}_paired.json",
        report = report(
                "results/trimmed/{sample}-{unit}_paired_fastp.html",
                caption="report/fastp.rst",
                category="trimmed (fastp)"
            )  
    envmodules:
        config["modules"]["fastp"] 
    singularity:
        config["containers"]["fastp"]
    threads:
        config["threads"]
    params:
        threads = config["threads"]
    shell:""" 
        fastp  --thread {params.threads}  --in1 {input[0]} --in2 {input[1]} --out1 {output.read1} --out2  {output.read2} --html {output.report} --json {output.json}
    """


rule fastp_single:
    input:
        get_fastq
    output:  
        read1="results/trimmed/{sample}-{unit}.trimmed.fastq.gz",
        json="results/trimmed/{sample}-{unit}.single.json",
        report = report(
                "results/trimmed/{sample}-{unit}_single_fastp.html",
                caption="report/fastp.rst",
                category="trimmed (fastp)"
            )
    envmodules:
        config["modules"]["fastp"] 
    singularity:
        config["containers"]["fastp"]
    threads:
        config["threads"]
    params:
        threads = config["threads"]
    shell:"""
        fastp --thread {params.threads}  --in1 {input[0]}  --out1 {output.read1} --html {output.report} --json {output.json}
    """    

# Prepare the genome for bisulfite sequencing
# This rule runs the bismark_genome_preparation script to create a bisulfite
# converted genome and its index. The bisulfite converted genome is required
# for downstream bisulfite sequencing analysis.
rule bismark_genome_preparation:
    """
    Prepare the genome for bisulfite sequencing

    This rule runs the bismark_genome_preparation script to create a bisulfite
    converted genome and its index. The bisulfite converted genome is required
    for downstream bisulfite sequencing analysis.

    """
    input:
        genome = rules.genome_fasta.output 
    output:
        directory("results/{reference}/Bisulfite_Genome")
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]
    params:
        outdir = "results/{reference}",
        aligner = config["bismark"]["aligner"],
        threads = config["threads"]
    threads:
        config["threads"]
    shell:"""
        bismark_genome_preparation --{params.aligner} --parallel {params.threads} {params.outdir}
    """

rule bismark:
    """
    Aligns the trimmed reads to the bisulfite converted genome using bismark

    The rule takes as input the bisulfite converted genome, the index, and the
    trimmed reads. It runs the bismark aligner to align the trimmed reads to the
    bisulfite converted genome. The output is a bam file containing the aligned
    reads and a report file containing information about the alignment.
 
    """
    input:
        genome =  rules.genome_fasta.output ,
        index = rules.bismark_genome_preparation.output,
        trimmed_read = get_fastp_files
    output:
        bam = "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.bam",
        report =  "results/{reference}/{sample}-{unit}_R1_bismark_bt2_PE_report.txt"
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]  
    threads:
        config["bismark"]["threads"] * 2
    params:
        mapper = config["bismark"]["aligner"],
        threads = config["bismark"]["threads"] * 2,
        outdir = "results/{reference}"
    shell:"""
         bismark --non_directional --genome  {params.outdir} -p {params.threads} --temp_dir {params.outdir} --bowtie2 -1 {input.trimmed_read[0]} -2  {input.trimmed_read[1]}  -o {params.outdir}
    """
    
rule deduplicate_bismark:
    """
    Deduplicates the aligned reads

    This rule runs the deduplicate_bismark script to remove PCR duplicates from the aligned reads.
    The output is a bam file containing the deduplicated reads and a report file containing information
    about the deduplication process. 

    """
    input: 
        rules.bismark.output.bam 
    output: 
        bam = "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.bam",
        report = "results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplication_report.txt"
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]
    threads:
        config["threads"]
    params:
        outdir = "results/{reference}"
    shell:"""
        deduplicate_bismark --paired --bam {input} --output_dir {params.outdir} 
    """

rule samtools_sort:
    """
    Sort the deduplicated bam file by read name

    This rule runs samtools sort to sort the deduplicated bam file by read name.
    The output is a sorted bam file and its index file.
 
    """
    input:
        bam=rules.deduplicate_bismark.output.bam
    output:
        bam="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.sorted.bam",
        bai="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.sorted.bam.bai"
    envmodules:
        config["modules"]["samtools"]
    singularity:
        config["containers"]["samtools"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    shell:"""
        # Sort the bam file by read name
        samtools sort  --threads {params.threads} {input} > {output.bam}
        # Create the index file
        samtools index {output.bam}
    """

rule bismark_methylation_extractor:
    """
    Extract methylation information from the bismark alignment result

    This rule runs bismark_methylation_extractor to extract methylation information from the
    bismark alignment result. The output includes the methylation information in the three
    contexts (CpG, CHG and CHH) and the M-bias report.

    """
    input: 
        rules.deduplicate_bismark.output.bam 
    output:                      
        CHH="results/{reference}/CHH_context_{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.txt.gz",
        CHG="results/{reference}/CHG_context_{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.txt.gz",
        CpG="results/{reference}/CpG_context_{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.txt.gz",
        mbias_report="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.M-bias.txt",
        coverage="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.bismark.cov.gz",
        bedgraph="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.bedGraph.gz",
        splitting_report="results/{reference}/{sample}-{unit}_R1_bismark_bt2_pe.deduplicated_splitting_report.txt"
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]
    threads:
        config["threads"] 
    params:
        outdir = "results/{reference}",
        threads = config["threads"]
    shell:"""
        # Extract methylation information from the bismark alignment result
        bismark_methylation_extractor --genome_folder {params.outdir} --bedGraph   --comprehensive --CX  --cytosine_report  --report --gzip --buffer_size 50%  --paired-end {input}  --parallel {params.threads}  --output_dir {params.outdir} 
    """

rule bismark2bedGraph:
    """
    Convert bismark output to bedGraph format

    This rule uses bismark2bedGraph to convert the bismark output to a bedGraph file, which
    is a format that can be easily visualized in a IGV genome browser.

    """
    input:
        "results/{reference}/{context}_context_{sample}-{unit}_R1_bismark_bt2_pe.deduplicated.txt.gz"
    output:
        coverage1 = "results/{reference}/{context}_{sample}-{unit}.cov.gz",
        coverage2 = "results/{reference}/{context}_{sample}-{unit}.cov.gz.bismark.cov.gz",
        txt = "results/{reference}/{context}_{sample}-{unit}.cov.txt"
    threads: 4

    params:
        outdir = "results/{reference}",
        unzip = "{context}_{sample}-{unit}.cov"
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]  
    shell:"""
        # Convert bismark output to bedGraph format
        bismark2bedGraph -o {params.unzip} -CX -dir {params.outdir} {input};
        # Unzip the output file and save it as a text file
        gzip -dc {output.coverage2} > {output.txt}
    """

rule methylkit_processBismarkAln:
    input:
        bam=rules.samtools_sort.output.bam,
        bai=rules.samtools_sort.output.bai
    output:
        "results/{reference}/methylkit/{sample}-{unit}_{context}.txt"
    params: 
        species = config["methylkit"]["species"],
        sample = "{sample}-{unit}",
        outdir = "results/{reference}/methylkit"
    threads:
        config["threads"]
    singularity:
        config["containers"]["methylkit"]
    script:
        "./scripts/processBismarkAln.R"
    

rule bamCoverage_individual:
    input:
        bam=rules.samtools_sort.output.bam,
        bai=rules.samtools_sort.output.bai
    output:
        "results/{reference}/{sample}-{unit}_coverage.bw"
    envmodules:
        config["modules"]["deeptools"]
    singularity:
        config["containers"]["deeptools"]
    threads:
        config["threads"]
    params: 
        threads = config["threads"],
        binsize = config["deeptools"]["binsize"]
    shell:"""
        bamCoverage -b {input.bam} -o {output}  --binSize {params.binsize} -p {params.threads}
    """


rule bismark2report:
    input: 
        report_bismark = rules.bismark.output.report, 
        report_deduplicate = rules.deduplicate_bismark.output.report,
        mbias_report =  rules.bismark_methylation_extractor.output.mbias_report, 
        splitting_report = rules.bismark_methylation_extractor.output.splitting_report
    output:   
        report = report(
                "results/{reference}/{sample}-{unit}_R1_bismark_bt2_PE_report.html",
                caption="report/bismark.rst",
                category="Report (bismark)"
        )
    envmodules:
        config["modules"]["bismark"]
    singularity:
        config["containers"]["bismark"]
    params:
        outdir = "results/{reference}"
    shell:"""
        bismark2report --dir {params.outdir} --alignment_report {input.report_bismark}
    """

 
rule methylkit:
    input:
        treatment = lambda w: list(set(
            expand(
                "results/{reference}/methylkit/{sample}-{unit}_{{context}}.txt",
                zip,
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                reference= genomes.keys()
            )
        )),
        control = lambda w: list(set(
            expand(
                "results/{reference}/methylkit/{control}-{unit}_{{context}}.txt",
                zip,
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample]["control"],
                reference= genomes.keys()
            )
        ))
    output:
        bedgraph_all = "results/{reference}/methylkit_results/{sample}_{control}_{context}.bedGraph",
        bedgraph_hyper = "results/{reference}/methylkit_results/{sample}_{control}_{context}.hyper.bedGraph",
        bedgraph_hypo = "results/{reference}/methylkit_results/{sample}_{control}_{context}.hypo.bedGraph",
        correlation   = report("results/{reference}/methylkit_results/{sample}_{control}_{context}-correlation-stats.pdf",category="stats"),
        clusters   = report("results/{reference}/methylkit_results/{sample}_{control}_{context}-clusters.pdf",category="stats"),
        pca   = report("results/{reference}/methylkit_results/{sample}_{control}_{context}-pca.pdf",category="stats")

    params:
        outdir = "results/{reference}/methylkit_results",
        threads = config["threads"],
        method = config["methylkit"]["method"],
        windowSize = config["methylkit"]["windowSize"],
        stepSize = config["methylkit"]["stepSize"],
        test = config["methylkit"]["test"],
        overdispersion = config["methylkit"]["overdispersion"],
        qvalue = config["methylkit"]["qValue"],
        minCov = config["methylkit"]["minCov"],
        minDiff = config["methylkit"]["minDiff"],
        context = "{context}",
        species = config["methylkit"]["species"],
        treatment = "{sample}_{context}",
        control = "{control}_{context}" 
    threads:
        config["threads"]
    singularity:
        config["containers"]["methylkit"] 
    script:
	    "./scripts/methylkit.R"
 
rule methylkit2:
    input:
        treatment = lambda w: list(set(
            expand(
                "results/{reference}/{{context}}_{sample}-{unit}.cov.txt",
               # zip, 
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                reference= genomes.keys()
            )
        )),
        control = lambda w: list(set(
            expand(
                "results/{reference}/{{context}}_{control}-{unit}.cov.txt", 
                #zip, 
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample]["control"],
                reference= genomes.keys()
            )
        ))
    output:
        bedgraph_all = "results/{reference}/methylkit_results2/{sample}_{control}_{context}.bedGraph",
        bedgraph_hyper = "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hyper.bedGraph",
        bedgraph_hypo = "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hypo.bedGraph",
        correlation   = report("results/{reference}/methylkit_results2/{sample}_{control}_{context}-correlation-stats.pdf",category="stats"),
        clusters   = report("results/{reference}/methylkit_results2/{sample}_{control}_{context}-clusters.pdf",category="stats"),
        pca   = report("results/{reference}/methylkit_results2/{sample}_{control}_{context}-pca.pdf",category="stats")

    params:
        outdir = "results/{reference}/methylkit_results2",
        threads = config["threads"],
        method = config["methylkit"]["method"],
        windowSize = config["methylkit"]["windowSize"],
        stepSize = config["methylkit"]["stepSize"],
        test = config["methylkit"]["test"],
        overdispersion = config["methylkit"]["overdispersion"],
        qvalue = config["methylkit"]["qValue"],
        minCov = config["methylkit"]["minCov"],
        minDiff = config["methylkit"]["minDiff"],
        context = "{context}",
        species = config["methylkit"]["species"],
        treatment = "{sample}_{context}",
        control = "{control}_{context}" 
    threads:
        config["threads"]
    singularity:
        config["containers"]["methylkit"] 
    script:
	    "./scripts/methylkit2.R"

rule peak_annotation:
    input:
        bed_hyper = rules.methylkit2.output.bedgraph_hyper,
        bed_hypo = rules.methylkit2.output.bedgraph_hypo,
        index = rules.samtools_index.output,
        gff = lambda wildcards: genomes[wildcards.reference]["gff3"],
    output:
        bed_hyper = "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hyper.txt",
        bed_hypo = "results/{reference}/methylkit_results2/{sample}_{control}_{context}.hypo.txt"
    singularity:
        config["containers"]["homer"]
    shell:"""
        annotatePeaks.pl {input.bed_hyper} {input.index} -gff3 {input.gff} > {output.bed_hyper};
        annotatePeaks.pl {input.bed_hypo} {input.index} -gff3 {input.gff} > {output.bed_hypo}
    """


rule multiqc:
    input:
        get_multiqc_input
    output:
        "results/multiqc/multiqc.html"
    singularity:
        config["containers"]["multiqc"]
    envmodules:
        config["modules"]["multiqc"],
        config["modules"]["python"]
    log:
        "logs/multiqc.log"
    params:
        directory = "results/multiqc",
        name = "multiqc.html"
    shell:
        "multiqc --force -o {params.directory} -n {params.name} {input} &> {log}"
