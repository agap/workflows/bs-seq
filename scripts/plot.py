import pandas as pd
import matplotlib.pyplot as plt
import sys

# Initialize an empty list to store annotations
annotations = []

# Open the file and extract the annotations
with open(sys.argv[1], 'r') as annotation_data:
    next(annotation_data)
    for line in annotation_data:
        columns = line.strip().split('\t')
        if len(columns) >= 8:
            annotation = columns[7].split('(')[0].strip()
            annotations.append(annotation)

# Convert list to a pandas Series and count the number of occurrences of each annotation
annotations_series = pd.Series(annotations)
features_counts = annotations_series.value_counts()

# Generate pie chart
plt.pie(features_counts.values, labels=features_counts.index, autopct="%.1f%%", wedgeprops={'edgecolor': 'white', 'linewidth': 0.5})
plt.title('Percentage of peak distribution')
plt.legend(loc='lower right', bbox_to_anchor=(1.35, 0.7))
plt.savefig(sys.argv[2])
plt.show()
