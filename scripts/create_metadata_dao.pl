#!/usr/bin/perl
use File::Basename;
use POSIX;
my $directory = shift;
my $technology = shift;


my %dao = (
    "SRR22122559" => "Azucena-meiocyte",
    "SRR22122560" => "Azucena-meiocyte",
    "SRR22122566" => "IR64-meiocyte",
    "SRR22122567" => "IR64-meiocyte" 
);
open(IN,"ls $directory/*gz|");
my %igg;
my %sample;
while(<IN>){
    chomp;
    my ($name,$path,$suffix) = fileparse($_,".fastq.gz");
    my ($id,$read) = (split(/\_/,$name));
    if ($dao{$id}){
        $id = $dao{$id}; 
   
        my $uniq = $id; 
        push @{$sample{$id}{$read}} , $_;
    } 
 
}
close IN;
print join("\t","sample","unit","fq1","fq2","control"),"\n";
foreach my $id (sort keys %sample) { 
  
    my @file = @{$sample{$id}{"1"}};
    for (my $i = 0;$i<=$#file;$i++)  { 
        if ($id eq "Azucena-meiocyte"){
            $control = "IR64-meiocyte";
        }
        else {
            $control = "Azucena-meiocyte";
        }
        print join("\t",$id,$i+1,$sample{$id}{"1"}[$i],$sample{$id}{"2"}[$i],$control) ,"\n";
    } 

}
 