#!/usr/bin/env Rscript

library(methylKit)
library("tools")

control = c("results/Nipponbare/MS-AZU-TS-1_R1_bismark_bt2_pe.deduplicated.sorted.bam","results/Nipponbare/MS-AZU-TS-2_R1_bismark_bt2_pe.deduplicated.sorted.bam")
treat = c("results/Nipponbare/MS-HYB-TS-1_R1_bismark_bt2_pe.deduplicated.sorted.bam","results/Nipponbare/MS-HYB-TS-2_R1_bismark_bt2_pe.deduplicated.sorted.bam")
# treatment
files  <- as.list(c(control, treat))


methRaw = processBismarkAln( files,sample.id=list("MS-AZU-TS-1","MS-AZU-TS-2","MS-HYB-TS-1","MS-HYB-TS-2"),treatment = c(0,0,1,1), assembly="rice",read.context="CpG", save.folder= "methylkit2/MS-HYB-AZU_CpG")

