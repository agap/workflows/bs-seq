# Snakemake workflow: BS-Seq data analysis using bismark

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)    
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
  

**Table of Contents** 

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Prerequisites](#prerequisites)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)
  - [Run workflow](#run-workflow)
  - [Output](#output)

## Objective

BS-Seq data analysis using bismark

## Dependencies

* fastp : A tool designed to provide fast all-in-one preprocessing for FastQ files This tool is developed in C++ with multithreading supported to afford high performance (https://github.com/OpenGene/fastp)
* bismark : Bismark is a set of tools for the time-efficient analysis of Bisulfite-Seq (BS-Seq) data (https://felixkrueger.github.io/Bismark/)
* samtools : Utilities for the Sequence Alignment/Map (SAM) format (http://www.htslib.org/doc/samtools.html)
* deeptools : Tools for exploring deep sequencing data. (https://deeptools.readthedocs.io/en/develop/index.html)
* methylkit : DNA methylation analysis from high-throughput bisulfite sequencing results (https://bioconductor.org/packages/release/bioc/html/methylKit.html) 

## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+

## Overview of programmed workflow

<p align="center">
<img src="images/rulegraph.svg">
</p>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/agap/workflows/bs-seq.git
  ```


- Change directories into the cloned repository:
  ```
  cd bs-seq
  ```

- Edit the configuration file config.yaml

```

metadata: "metadata.tsv"
reference: 
  Nipponbare: 
    assembly: "/storage/replicated/cirad/web/HUBs/rice2/Oryza_sativa_japonica_Nipponbare/Oryza_sativa_japonica_Nipponbare.assembly.fna"
    gff3: "/storage/replicated/cirad/web/HUBs/rice2/Oryza_sativa_japonica_Nipponbare/Oryza_sativa_japonica_Nipponbare.gff3"


```


- Create a file metadata.tsv

|sample  |unit  |fq1  |fq2  |control|
|--|--|--|--|--|
|MS-AZU-Mei  |1  |/path/to/fastq/MS-AZU-Mei-1/R1  |/path/to/fastq/MS-AZU-Mei-1/R2  |MS-AZU-TS |
|MS-AZU-Mei  |2  |/path/to/fastq/MS-AZU-Mei-2/R1  |/path/to/fastq/MS-AZU-Mei-2/R2  |MS-AZU-TS  |
|MS-AZU-TS  |1  |/path/to/fastq/MS-AZU-TS-1/R1  |/path/to/fastq/MS-AZU-TS-1/R2  |MS-AZU-TS  |
|MS-AZU-TS  |2  |/path/to/fastq/MS-AZU-TS-2/R1  |/path/to/fastq/MS-AZU-TS-2/R2  |MS-AZU-TS  |

 For this, you use scripts/create_metadata.pl to generate this file
 
 ```bash
 scripts/create_metadata.pl /storage/replicated/cirad/projects/LANDSREC/raw_data/CUTandTag/BHKGMGDRX3/ > metadata.tsv
 ```
 
## Run workflow

- Print shell commands to validate your modification (mode dry-run)

```bash
sbatch snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```

- Unlock directory

```bash
sbatch snakemake.sh unlock
```

- Generate DAG file

```bash
sbatch snakemake.sh dag
```


## Output 

