#!/bin/bash 
#
#SBATCH -J methylation
#SBATCH -o methylation."%j".out
#SBATCH -e methylation."%j".err 

#SBATCH -p agap_long


module purge

module load snakemake/7.15.1-conda
module load singularity/3.6.3

# Print shell commands
#snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-singularity --cores 1

# Unlock repository if one job failed
# snakemake   --profile profile --jobs 200 --unlock  --use-singularity

# Create DAG file
#snakemake  --profile profile --jobs 2 --rulegraph  --use-singularity | dot -Tsvg > images/rulegraph.svg

# Run workflow using envmodules
snakemake --profile profile --jobs 60   -p  --use-singularity
 
